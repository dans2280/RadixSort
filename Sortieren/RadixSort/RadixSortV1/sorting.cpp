//If somebody is interested into the theory, i recommend:
//https://www.geeksforgeeks.org/radix-sort/

//TODO: Test
//TODO: write header
//TODO: finish Implementation

#include "sorting.h"
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

vector<int> sort( vector <int> intList );
vector<int> sortStep( vector<int> intList, int dPointer );
int getDigit( int val, int dPointer);
vector<int> mergeVectors( vector<int> intList, vector<int> attachable);

//begin of the radixSort
vector<int> sort( vector <int> intList ){
    //looks which is the largest Number of intList to stop at the last used digit
    int maxValue { *max_element( intList.cbegin(), intList.cend() ) };
    int lastdigit { static_cast<int> ( log10( maxValue ) )};

    for(int i{0}; i < lastdigit;i++ ){
        intList = sortStep( intList, i );
    }
    return intList;
}

//Sorting the list by looking at one specific digit of each number
//dPointer => digit pointer
//dPointer shows which digit is important for the sorting
vector<int> sortStep( vector<int> intList, int dPointer ){
    //split the vector into 10 vectors
    vector<vector <int> >newList;
    for( int i {0}; i <= 9; i++ ){
        newList.push_back( vector<int>() );
    }
    for( int val : intList ){
            newList.at( static_cast<ulong>( getDigit( val, dPointer ) ) ).push_back( val );
    }
    intList.clear();

    //merges all vectors back into one
    for( int i {0}; i <= 9; i++ ){
        intList = mergeVectors( intList, newList.at( static_cast<ulong> ( i ) ) );
    }

    return intList;
}

//get a specific digit from a number
int getDigit( int val, int dPointer){
    int digit;
    int x { static_cast<int> ( pow( 10, dPointer + 1 ) ) };
    int y { static_cast<int> ( pow( 10, dPointer) ) };

    val = val % x;
    digit = val / y ;
    return digit;
}

//merges 2 vectors together
vector<int> mergeVectors( vector<int> intList, vector<int> attachable ){
    for( int element : attachable ){
        intList.push_back( element );
    }
    return intList;
}
