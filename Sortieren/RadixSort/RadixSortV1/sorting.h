#ifndef SORTING_H
#define SORTING_H

#include <vector>

using namespace std;

vector<int> sort( vector <int> intList );

#endif // SORTING_H
